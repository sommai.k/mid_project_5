// import { View } from 'react-native';
// import { FC, useState } from 'react';
// import { HelloWorld } from './components/HelloWorld';
// import { CountButton } from './components/CountButton';

// const App: FC = () => {
//   const [cnt, setCnt] = useState<number>(0);

//   const whenCntClick = () => {
//     setCnt((state) => state = state + 1);
//   }
//   return (
//     <View className="bg-gray-200 w-screen h-screen p-8">
//       <View className='m-auto'>
//         <HelloWorld msg={`Hello World  ${cnt}`} />
//         <CountButton label='OK' whenClick={whenCntClick} />
//       </View>
//     </View>
//   );
// }

// export default App;

import { FC } from 'react';
import { ScreenManager } from './components/screens/ScreenManager';
import { Provider } from 'react-redux';
import store from './app/store';

const App: FC = () => {
  return <Provider store={store}>
    <ScreenManager />
  </Provider>
}

export default App;