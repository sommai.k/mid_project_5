import { PayloadAction, createSlice } from '@reduxjs/toolkit';

const screenSlice = createSlice({
    name: 'screen',
    initialState: {
        screenName: 'MainApp',
        screenId: 0
    },
    reducers: {
        goto: (state, action: PayloadAction<string>) => {
            state.screenName = action.payload;
        },
        setScreenId: (state, action: PayloadAction<number>) => {
            state.screenId = action.payload;
        }
    }
});

export const { goto, setScreenId } = screenSlice.actions;
export const screenReducer = screenSlice.reducer;