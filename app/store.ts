import { configureStore } from '@reduxjs/toolkit';
import { authReducer } from './auth-slice';
import { screenReducer } from './screen-slice';

export default configureStore({
    reducer: {
        auth: authReducer,
        screen: screenReducer,
    }
});