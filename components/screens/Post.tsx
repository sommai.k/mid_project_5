import { FC, useEffect, useState } from 'react';
import { SafeAreaView, Text, View, Alert, ScrollView } from 'react-native';
import axios from 'axios';
import { MyAppTitle } from '../shared/MyAppTitle';

const api = axios.create();

interface Post {
    userId: number;
    id: number;
    title: string;
    body: string;
}

interface Props {
    item: Post;
    index: number;
}

// Local component
const PostItem: FC<Props> = (props) => {
    return <View key={`post-item-${props.index}`}
        className='flex flex-row rounded-lg items-center p-2 space-x-2 mt-2 bg-blue-100 shadow-sm'>
        <Text className='text-xl'>{props.item.id}</Text>
        <View className='flex flex-col p-2'>
            <Text className='text-xl'>{props.item.title}</Text>
            <Text className='text-sm'>{props.item.body}</Text>
        </View>
    </View>
}

export const Post: FC = () => {
    const [posts, setPosts] = useState<Post[]>([]);

    useEffect(() => {
        // business logic here
        loadData();
    }, []);

    // load data from server
    const loadData = async () => {
        try {
            const resp = await api.get('https://jsonplaceholder.typicode.com/posts');
            console.log(resp.data);
            setPosts(resp.data);
        } catch (e) {
            console.log(e);
            Alert.alert('พบข้อผิดพลาด', 'กรุณาลองอีกครั้งหนึ่ง');
        }
    }

    return (
        <SafeAreaView>
            <MyAppTitle label='บทความ' />
            <ScrollView className='px-2 flex flex-col android:h-screen'>
                {
                    posts.map((value, index) => <PostItem index={index} item={value} />)
                }
            </ScrollView>
        </SafeAreaView>
    )
};