import { FC } from 'react';
import { View, Image, Text, TextInput, Pressable } from 'react-native';
import { z } from 'zod';
import { SubmitErrorHandler, SubmitHandler, useForm, Controller } from 'react-hook-form';
import { zodResolver } from "@hookform/resolvers/zod";
import { MyInput } from '../shared/MyInput';
import { MyButton } from '../shared/MyButton';
import { useDispatch } from 'react-redux';
import { login } from '../../app/auth-slice';

const LoginSchema = z.object({
    email: z.string().email(),
    password: z.string().min(5)
});

type LoginModel = z.infer<typeof LoginSchema>;

export const Login: FC = () => {

    const dispatch = useDispatch();

    const { handleSubmit, control, reset } = useForm<LoginModel>({
        resolver: zodResolver(LoginSchema),
    });

    // when validate pass
    const whenValidatePass: SubmitHandler<LoginModel> = (loginForm) => {
        console.log(loginForm.email, loginForm.password);
        dispatch(login());
    }

    // when validate fail
    const whenValidateFail: SubmitErrorHandler<LoginModel> = (errors) => {
        console.log(errors);
    }

    return <View className='flex flex-col h-screen px-4 pt-[50px] space-y-2'>
        <Image source={require('../../assets/node-logo.png')}
            className='w-full' />
        <Text className='text-3xl text-center mt-5'>Welcome to my system</Text>
        <MyInput control={control} name="email" label="อีเมล์" />
        <MyInput control={control} name="password" label="รหัสผ่าน" isPassword={true} />
        <View className='flex flex-row space-x-1'>
            <View className='flex-1'>
                <MyButton
                    label='เข้าสู่ระบบ'
                    whenButtonPress={handleSubmit(whenValidatePass, whenValidateFail)} />
            </View>
            <View className='flex-1'>
                <MyButton label="รีเซ็ท"
                    whenButtonPress={() => reset({ email: '', password: '' })} />
            </View>
        </View>
    </View>
}