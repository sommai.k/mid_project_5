import { FC, useEffect, useState } from 'react';
import { Alert, SafeAreaView, ScrollView, Text, View } from 'react-native';
import axios from 'axios';
import { AnimatedIcon } from '../shared/AnimatedIcon';

const api = axios.create();

interface Comment {
    postId: number;
    id: number;
    name: string;
    email: string;
    body: string;
}

interface Props {
    item: Comment;
    index: number;
}

const CommentItem: FC<Props> = (props) => {
    return <View key={`comment-item-${props.index}`} className='border-b-2 bg-blue-100 p-2'>
        <Text className="text-">{props.item.name} [{props.item.email}]</Text>
        <Text>{props.item.body}</Text>
    </View>
}

export const Message: FC = () => {
    const [comments, setComments] = useState<Comment[]>([]);
    const [isLoading, setIsLoading] = useState<boolean>(false);

    useEffect(() => {
        // business logic here
        loadData();
    }, []);

    // load data from server
    const loadData = async () => {
        try {
            setIsLoading(true);
            const resp = await api.get('https://jsonplaceholder.typicode.com/comments');
            setComments(resp.data);
        } catch (e) {
            Alert.alert('พบข้อผิดพลาด', 'กรุณาลองอีกครั้งหนึ่ง');
        } finally {
            setIsLoading(false);
        }
    }
    return <SafeAreaView className='android:mt-8'>
        {
            isLoading && <View className='w-[40px] h-[40px]'>
                <AnimatedIcon />
            </View>
        }
        <ScrollView className='px-2 flex flex-col andriod:h-full android:bottom-20 ios:bottom-9'>
            {
                comments.map((value, index) => <CommentItem index={index} item={value} />)
            }
        </ScrollView>
    </SafeAreaView>
};