import { FC, useState } from 'react';
import { View, SafeAreaView } from 'react-native';
import { MyIconButton } from '../shared/MyIconButton';
import { Home } from './Home';
import { Menu } from './Menu';
import { Message } from './Message';
import { Setting } from './Setting';
import { useSelector, useDispatch } from 'react-redux';
import { setScreenId } from '../../app/screen-slice';

const appScreen = [
    <Home />,
    <Menu />,
    <Message />,
    <Setting />
];

export const MainApp: FC = () => {
    // const [screenId, setScreenId] = useState<number>(0);
    const screenId = useSelector((state: any) => state.screen.screenId);
    const dispatch = useDispatch();

    return <SafeAreaView className='w-screen h-full relative andriod:mt-[25px]'>
        {appScreen[screenId]}
        <View className='bottom-0 absolute flex flex-row  bg-blue-600 w-full'>
            <View className='w-1/4'>
                <MyIconButton label='Home' icon="home"
                    whenButtonPress={() => dispatch(setScreenId(0))} />
            </View>
            <View className='w-1/4'>
                <MyIconButton label='Menu' icon="menu"
                    whenButtonPress={() => dispatch(setScreenId(1))} />
            </View>
            <View className='w-1/4'>
                <MyIconButton label='Message' icon="chatbox"
                    whenButtonPress={() => dispatch(setScreenId(2))} />
            </View>
            <View className='w-1/4'>
                <MyIconButton label='Setting' icon="settings"
                    whenButtonPress={() => dispatch(setScreenId(3))} />
            </View>
        </View>
    </SafeAreaView>
};