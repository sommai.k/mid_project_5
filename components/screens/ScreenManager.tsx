import { FC } from 'react';
import { useSelector } from 'react-redux';
import { Text } from 'react-native';
import { MainApp } from './MainApp';
import { Login } from './Login';
import { Post } from './Post';

export const ScreenManager: FC = () => {
    const isLogedIn = useSelector((state: any) => state.auth.isLogedIn);
    const screenName = useSelector((state: any) => state.screen.screenName);
    const showScreen = () => {
        if (isLogedIn) {
            switch (screenName) {
                case 'MainApp': return <MainApp />;
                case 'Post': return <Post />
                default: return <Text>Screen not found !!!</Text>
            }
        } else {
            return <Login />
        }
    }
    return <>{showScreen()}</>
}