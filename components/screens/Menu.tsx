import { FC } from 'react';
import { View } from 'react-native';
import { MyIconButton } from '../shared/MyIconButton';
import { useDispatch } from 'react-redux';
import { goto } from '../../app/screen-slice';

interface Menu {
    label: string;
    icon: string;
    screen: string;
}

const menus: Menu[] = [{
    label: 'Post',
    icon: 'chatbubbles',
    screen: 'Post'
}, {
    label: 'Comments',
    icon: 'at',
    screen: ''
}, {
    label: 'Album',
    icon: 'albums',
    screen: ''
}, {
    label: 'Photo',
    icon: 'camera',
    screen: ''
}, {
    label: 'Todo',
    icon: 'list',
    screen: ''
}, {
    label: 'User',
    icon: 'person',
    screen: ''
}];

export const Menu: FC = () => {

    const dispatch = useDispatch();

    return <View className='android:mt-8 bg-gray-700 h-full px-2'>
        <View className='flex flex-row flex-wrap  '>
            {
                menus.map((menu, index) =>
                    <View className='w-1/4' key={index}>
                        <MyIconButton
                            label={menu.label}
                            size={36}
                            icon={menu.icon}
                            whenButtonPress={() => dispatch(goto(menu.screen))}
                        />
                    </View>
                )
            }
        </View>
    </View>
};