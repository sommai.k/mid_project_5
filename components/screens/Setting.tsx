import { FC } from 'react';
import { Text, View } from 'react-native';
import { MyButton } from '../shared/MyButton';
import { useDispatch } from 'react-redux';
import { logout } from '../../app/auth-slice';

export const Setting: FC = () => {
    const dispatch = useDispatch();
    //when logout
    const whenLogout = () => {
        dispatch(logout());
    }
    return <View>
        <View className='mt-8 flex flex-col'>
            <Text>Setting</Text>
            <MyButton label="ออกจากระบบ" whenButtonPress={whenLogout} />
        </View>
    </View>
};