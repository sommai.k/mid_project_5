import { FC } from 'react';
import { Pressable, View, Text } from 'react-native';

interface Props {
    whenClick: () => void;
    label: string;
}

export const CountButton: FC<Props> = (props) => {
    return <Pressable onPress={props.whenClick}>
        <View className='bg-blue-200 p-2 border rounded-lg border-gray-200'>
            <Text className='text-center text-lg text-gray-100 font-bold'>{props.label}</Text>
        </View>
    </Pressable>
}
