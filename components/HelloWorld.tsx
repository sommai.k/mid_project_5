import { FC } from 'react';
import { Text } from 'react-native';

interface Props {
    msg: String
}

export const HelloWorld: FC<Props> = (props) => {
    return <Text className='text-xl'>{props.msg}</Text>
}