import { FC } from 'react';
import { Control, Controller } from 'react-hook-form';
import { Text, TextInput } from 'react-native';

interface Props {
    label: string;
    name: string;
    control: Control<any>;
    isPassword?: boolean;
}

export const MyInput: FC<Props> = (props) => {
    return <>
        <Text className='text-lg mt-3'>{props.label}</Text>
        <Controller
            control={props.control}
            name={props.name}
            render={({ field }) =>
            (
                <TextInput
                    className='border p-3 rounded-xl border-blue-400'
                    value={field.value}
                    onChangeText={field.onChange}
                    secureTextEntry={props.isPassword}
                />
            )
            }
        />
    </>
}