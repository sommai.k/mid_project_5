import { FC } from 'react';
import { Text, Pressable } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

interface Props {
    icon: any;
    label: string;
    whenButtonPress: () => void;
    size?: number;
}

export const MyIconButton: FC<Props> = (props) => {
    return <Pressable
        className='items-center space-y-1 active:bg-blue-300 p-3 rounded-md'
        onPress={props.whenButtonPress}>
        <Ionicons
            name={props.icon} size={props.size ?? 24} color="white" />
        <Text className='text-white'>{props.label}</Text>
    </Pressable>
};