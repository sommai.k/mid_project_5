import { FC } from 'react';
import { Pressable, Text } from 'react-native';

interface Props {
    label: string;
    whenButtonPress: () => void;

}

export const MyButton: FC<Props> = (props) => {
    return <Pressable
        className={'bg-blue-600 p-2 rounded-lg'}
        onPress={props.whenButtonPress}>
        <Text className='text-white text-center'>{props.label}</Text>
    </Pressable>
} 