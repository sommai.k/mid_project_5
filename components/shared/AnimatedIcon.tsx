import { FC } from 'react'
import { Animated, Easing, StyleProp, TextStyle } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

interface Props {
  styles?: StyleProp<TextStyle>
}

export const AnimatedIcon: FC<Props> = (props) => {
  const spinValue = new Animated.Value(0);

  Animated.loop(
    Animated.timing(spinValue, {
      toValue: 1,
      duration: 500,
      easing: Easing.linear,
      useNativeDriver: true,
    }),
  ).start(() => {
    spinValue.setValue(0);
  });

  const spin = spinValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  return (
    <Animated.View style={{ transform: [{ rotate: spin }] }} >
      <FontAwesome
        size={40}
        color={'blue'}
        name="spinner"
        style={props.styles}
      />
    </Animated.View>
  );
}