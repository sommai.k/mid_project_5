import { FC } from 'react';
import { View, Pressable, Text } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { useDispatch } from 'react-redux';
import { goto } from '../../app/screen-slice';

interface Props {
    label: string;
}

export const MyAppTitle: FC<Props> = (props) => {
    const dispatch = useDispatch();
    const whenBackPress = () => {
        dispatch(goto("MainApp"));
    }

    return <View className='bg-blue-600 flex flex-row android:mt-8 p-2 space-x-2'>
        <Pressable onPress={whenBackPress}>
            <Ionicons name="arrow-back" size={32} color="white" />
        </Pressable>
        <Text className='text-xl text-white'>{props.label}</Text>
    </View>
}