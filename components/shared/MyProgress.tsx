import { FC } from 'react';
import { View } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

export const MyProgress: FC = () => {
    return <View className="absolute top-0 left-0 h-screen w-screen z-40 flex bg-gray-50 opacity-80">
        <View className="w-fit h-fit m-auto z-50">
            <View className="inline-flex gap-2 items-center">
                <Ionicons name='home' size={48} color="red" style={{ transform: [{ rotateY: '180deg' }] }} />
            </View>
        </View>
    </View>

}